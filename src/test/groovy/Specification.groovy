import com.mockertim.revolut.backendtask.Constants
import com.mockertim.revolut.backendtask.Main
import spock.lang.Shared
import spock.lang.Specification

class TransferAPISpec extends Specification {

    static final port = 8080
    static final host = "localhost"

    @Shared main = new Main()

    def setupSpec() {
        main.startAPIHttpServer(port)
    }

    def cleanupSpec() {
        main.stopAPIHttpServer()
    }

    def "Money transfer transaction" () {
        given:
            def receiverAccountNumber = "00000000000000000000"
            def senderAccountNumber = "11111111111111111111"
            def senderName = "John Wick"
            def senderEmail = "j.wick@example.com"
            def senderPhoneNumber = ""
            def currency = "EUR"
            def sumToTransfer = 99.99
            URL url = new URL("http://"+host+":"+port+"/api/transfer/account");
            StringBuilder data = new StringBuilder()
            data.append("{")
            data.append("\"recAccount\": \""+receiverAccountNumber+"\",")
            data.append(" \"senderAccount\": \""+senderAccountNumber+"\",")
            data.append(" \"senderName\": \""+senderName+"\",")
            data.append(" \"currency\": \""+currency+"\",")
            data.append(" \"sumToTransfer\": \""+sumToTransfer+"\"")
            data.append("}")
            byte[] input = data.toString().getBytes("utf-8");
        when:
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST")
            conn.setRequestProperty(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON)
            conn.setRequestProperty(Constants.ACCEPT, Constants.APPLICATION_JSON)
            conn.setDoOutput(true)
            OutputStream os = conn.getOutputStream()
            os.write(input, 0, input.length);
        then:
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), "utf-8"))
            String line = reader.readLine()
            reader.close()
            line != null
            line.contains("transactionId")
            line.contains("transactionStatus")
            line.contains("transactionDate")
            System.out.println("resp: " + line)
    }

    def "Transactions history" () {
        given:
            URL url = new URL("http://"+host+":"+port+"/api/transfer/transactions");
        when:
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET")
        then:
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), "utf-8"))
            String line = reader.readLine()
            reader.close()
            line != null
            System.out.println("resp: " + line)
    }
}
