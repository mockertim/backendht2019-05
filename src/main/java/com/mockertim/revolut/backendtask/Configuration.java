package com.mockertim.revolut.backendtask;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mockertim.revolut.backendtask.exceptions.ExceptionHandler;

class Configuration {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private static final ExceptionHandler GLOBAL_ERROR_HANDLER
        = new ExceptionHandler(OBJECT_MAPPER);
    
    private static final InMemoryTransactionsDatastore DATA_STORE
            = new InMemoryTransactionsDatastore();

    private static final TransactionService SERVICE
            = new TransactionService(DATA_STORE);

    static ObjectMapper getObjectMapper() {
        return OBJECT_MAPPER;
    }

    public static InMemoryTransactionsDatastore getDataStore() {
        return DATA_STORE;
    }

    public static TransactionService getTransactionService() {
        return SERVICE;
    }

    public static ExceptionHandler getErrorHandler() {
        return GLOBAL_ERROR_HANDLER;
    }
}
