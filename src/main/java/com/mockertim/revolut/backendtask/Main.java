package com.mockertim.revolut.backendtask;

import com.mockertim.revolut.backendtask.handlers.MoneyTransferHandler;
import com.mockertim.revolut.backendtask.handlers.TransactionsListHandler;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.InetSocketAddress;
import java.util.Scanner;

import static com.mockertim.revolut.backendtask.Configuration.*;

/**
 * Main class of the Backend Home Task 2019-05 implementation
 * 
 * Created at 16:09 2019-09-17
 *
 * @author Timur Shakuov (t.shakuov at gmail.com)
 */
public class Main {

    public static String serverHost = "localhost";
    public static int serverPort = 8080;
    public static int maxConnections = 1000;

    public HttpServer server;

    /**
     * 
     * @param args
     */
    public static void main(String... args) {
        Main m = new Main();
        if (args.length > 0) {
            try {
                serverPort = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                System.err.println("Argument" + args[0] + " must be an integer."
                    + "Using default port " + serverPort + ".");
            }
        }
        try {
            m.startAPIHttpServer(serverPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * @throws IOException
     */
    public synchronized void startAPIHttpServer(int serverPort) throws IOException {
        server = HttpServer.create(
                new InetSocketAddress(serverPort), maxConnections);
        HttpContext defaultContext =
                server.createContext("/api/transfer", (exchange -> {
            String rm = exchange.getRequestMethod();
            if ("GET".equals(rm)) {
                File input = new File(this.getClass().getClassLoader()
                        .getResource("api_descr.html").getFile());
                String respText = new Scanner(input).useDelimiter("\\Z").next();
                exchange.sendResponseHeaders(200, respText.getBytes().length);
                OutputStream output = exchange.getResponseBody();
                output.write(respText.getBytes());
                output.flush();
            } else {
                exchange.sendResponseHeaders(405, -1);
            }
            exchange.close();
        }));
        MoneyTransferHandler ath = new MoneyTransferHandler(getTransactionService(),
                getObjectMapper(), getErrorHandler());
        HttpContext atContext =
                server.createContext("/api/transfer/account", ath::handle);
        TransactionsListHandler tlh = new TransactionsListHandler(getTransactionService(),
                getObjectMapper(), getErrorHandler());
        HttpContext tlContext =
                server.createContext("/api/transfer/transactions", tlh::handle);
        server.setExecutor(null); // creates a default executor
        server.start();
        System.out.println("APIHttpServer started.");
    }

    public synchronized void stopAPIHttpServer() {
        server.stop(100);
        System.out.println("APIHttpServer stopped.");
    }
}
