package com.mockertim.revolut.backendtask;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * Created at 13:35 2019-09-22
 *
 * @author Timur Shakuov (t.shakuov at gmail.com)
 */
public class TransactionService {
    private final InMemoryTransactionsDatastore datastore;

    public TransactionService(InMemoryTransactionsDatastore datastore) {
        this.datastore = datastore;
    }

    public void perform(Transaction t) {
        final String transactionId = UUID.randomUUID().toString()
                .replace("-", "");
        final Date transactionDate = Calendar.getInstance().getTime();
        boolean transactionStatus = false;
        t.setTransactionId(transactionId);
        t.setTransactionDate(transactionDate);
        t.setTransactionStatus("true");
        datastore.save(t);
    }

    public String getTransactionsList() {
        Collection<Transaction> transactions = datastore.getTransactions();
        StringBuilder tl = new StringBuilder();
        tl.append("{");
        for(Transaction t : transactions) {
            tl.append(t.toString() + ", ");
        }
        tl.delete(tl.length()-2, tl.length()-1);
        tl.append("}");
        return tl.toString();
    }
}
