package com.mockertim.revolut.backendtask;

public class ErrorResponseBuilder {

    private int code;
    private String message;

    public ErrorResponseBuilder() {}

    public ErrorResponseBuilder(ErrorResponse response) {
        this.code = response.getCode();
        this.message = response.getMessage();
    }

    public ErrorResponseBuilder message(String message) {
        this.message = message;
        return this;
    }

    public ErrorResponseBuilder code(int code) {
        this.code = code;
        return this;
    }

    public ErrorResponse build() {
        ErrorResponse animal = new ErrorResponse();
        animal.setCode(code);
        animal.setMessage(message);
        return animal;
    }
}
