package com.mockertim.revolut.backendtask;

import java.util.Date;

/**
 * Money transfer transaction data
 */
public class Transaction {
    /**
     * Receiver account number
     */
    String recAccount;

    /**
     * Sender account number
     */
    String senderAccount;

    /**
     * Sender name
     */
    String senderName;

    /**
     * Sender email
     */
    String senderEmail;

    /**
     * Sender phone number
     */
    String senderPhone;

    /**
     * Transfer currency
     */
    String currency;

    /**
     * Transfer amount
     */
    double sumToTransfer;

    /**
     * Transaction id
     */
    String transactionId;

    /**
     * Transaction status
     */
    String transactionStatus;

    /**
     * Transaction date
     */
    Date transactionDate;

    public String getRecAccount() {
        return recAccount;
    }

    public void setRecAccount(String recAccount) {
        this.recAccount = recAccount;
    }

    public String getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(String senderAccount) {
        this.senderAccount = senderAccount;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getSenderPhone() {
        return senderPhone;
    }

    public void setSenderPhone(String senderPhone) {
        this.senderPhone = senderPhone;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getSumToTransfer() {
        return sumToTransfer;
    }

    public void setSumToTransfer(double sumToTransfer) {
        this.sumToTransfer = sumToTransfer;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    @Override
    public String toString(){
        return "{"
                + "id: " + getTransactionId() + ", "
                + "status: " + getTransactionStatus() + ", "
                + "date: " + getTransactionDate() + ", "
                + "recAccount: " + getRecAccount() + ", "
                + "senderAccount: " + getSenderAccount() + ", "
                + "senderName: " + getSenderName() + ", "
                + "currency: " + getCurrency() + ", "
                + "sum: " + getSumToTransfer() + ""
                + "}";
    }
}
