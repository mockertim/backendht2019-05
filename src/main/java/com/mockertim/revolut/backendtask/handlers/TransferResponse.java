package com.mockertim.revolut.backendtask.handlers;

import java.util.Date;

/**
 * Money transfer response data
 */
public class TransferResponse {

    public TransferResponse(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * Transaction id
     */
    String transactionId;

    /**
     * Transaction status
     */
    String transactionStatus;

    /**
     * Transaction date
     */
    Date transactionDate;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    @Override
    public String toString(){
        return "{" + getTransactionId() + ", "
                + getTransactionStatus() + ", "
                + getTransactionDate() + "}";
    }
}
