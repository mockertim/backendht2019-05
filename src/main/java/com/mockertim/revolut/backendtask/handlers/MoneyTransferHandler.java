package com.mockertim.revolut.backendtask.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mockertim.revolut.backendtask.Constants;
import com.mockertim.revolut.backendtask.Transaction;
import com.mockertim.revolut.backendtask.TransactionService;
import com.mockertim.revolut.backendtask.exceptions.ApplicationExceptions;
import com.mockertim.revolut.backendtask.exceptions.ExceptionHandler;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 */
public class MoneyTransferHandler extends Handler {

    private final TransactionService service;

    public MoneyTransferHandler(TransactionService service, ObjectMapper objectMapper,
                                ExceptionHandler exceptionHandler) {
        super(objectMapper, exceptionHandler);
        this.service = service;
    }

    protected void execute(HttpExchange exchange) throws IOException {
        byte[] response;
        if ("POST".equals(exchange.getRequestMethod())) {
            ResponseEntity e = doPost(exchange.getRequestBody());
            exchange.getResponseHeaders().putAll(e.getHeaders());
            exchange.sendResponseHeaders(e.getStatusCode().getCode(), 0);
            response = writeResponse(e.getBody());
        } else {
            throw ApplicationExceptions.methodNotAllowed(
                    "Method " + exchange.getRequestMethod()
                    + " is not allowed for " + exchange.getRequestURI()).get();
        }
        OutputStream os = exchange.getResponseBody();
        os.write(response);
        os.close();
    }

    private ResponseEntity<TransferResponse> doPost(InputStream is) {
        TransferRequest transferRequest = readRequest(is, TransferRequest.class);

        Transaction t = new Transaction();
        t.setRecAccount(transferRequest.getRecAccount());
        t.setSenderAccount(transferRequest.getSenderAccount());
        t.setSenderName(transferRequest.getSenderName());
        t.setCurrency(transferRequest.getCurrency());
        t.setSumToTransfer(transferRequest.getSumToTransfer());

        service.perform(t);

        TransferResponse response = new TransferResponse(t.getTransactionId());
        response.setTransactionStatus(t.getTransactionStatus());
        response.setTransactionDate(t.getTransactionDate());

        return new ResponseEntity<>(response,
                getHeaders(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON),
                StatusCode.OK);
    }
}
