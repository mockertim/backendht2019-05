package com.mockertim.revolut.backendtask.handlers;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Money transfer request data
 */
public class TransferRequest {

    /**
     * Receiver account number
     */
    String recAccount;

    /**
     * Sender account number
     */
    String senderAccount;

    /**
     * Sender name
     */
    String senderName;

    /**
     * Sender email
     */
    @JsonIgnore
    String senderEmail;

    /**
     * Sender phone number
     */
    @JsonIgnore
    String senderPhone;

    /**
     * Transfer currency
     */
    String currency;

    /**
     * Transfer amount
     */
    double sumToTransfer;

    public String getRecAccount() {
        return recAccount;
    }

    public void setRecAccount(String recAccount) {
        this.recAccount = recAccount;
    }

    public String getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(String senderAccount) {
        this.senderAccount = senderAccount;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getSenderPhone() {
        return senderPhone;
    }

    public void setSenderPhone(String senderPhone) {
        this.senderPhone = senderPhone;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getSumToTransfer() {
        return sumToTransfer;
    }

    public void setSumToTransfer(double sumToTransfer) {
        this.sumToTransfer = sumToTransfer;
    }

    @Override
    public String toString(){
        return "{" + getRecAccount() + ", "
                + getSenderAccount() + ", "
                + getSenderName() + ", "
                + getSenderEmail() + ", "
                + getSenderPhone() + ", "
                + getCurrency() + ", "
                + getSumToTransfer() + "}";
    }
}
