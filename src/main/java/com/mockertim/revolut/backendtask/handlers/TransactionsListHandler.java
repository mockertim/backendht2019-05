package com.mockertim.revolut.backendtask.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mockertim.revolut.backendtask.Constants;
import com.mockertim.revolut.backendtask.Transaction;
import com.mockertim.revolut.backendtask.TransactionService;
import com.mockertim.revolut.backendtask.exceptions.ApplicationExceptions;
import com.mockertim.revolut.backendtask.exceptions.ExceptionHandler;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 */
public class TransactionsListHandler extends Handler {

    private final TransactionService service;

    public TransactionsListHandler(TransactionService service,
                                   ObjectMapper objectMapper, ExceptionHandler exceptionHandler) {
        super(objectMapper, exceptionHandler);
        this.service = service;
    }

    @Override
    protected void execute(HttpExchange exchange) throws IOException {
        byte[] response;
        if ("GET".equals(exchange.getRequestMethod())) {
            ResponseEntity e = doGet(exchange.getRequestBody());
            exchange.getResponseHeaders().putAll(e.getHeaders());
            exchange.sendResponseHeaders(e.getStatusCode().getCode(), 0);
            response = writeResponse(e.getBody());
        } else {
            throw ApplicationExceptions.methodNotAllowed(
                    "Method " + exchange.getRequestMethod()
                    + " is not allowed for " + exchange.getRequestURI()).get();
        }
        OutputStream os = exchange.getResponseBody();
        os.write(response);
        os.close();
    }

    private ResponseEntity<String> doGet(InputStream is) {
        String tl = service.getTransactionsList();
        return new ResponseEntity<>(tl,
                getHeaders(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON),
                StatusCode.OK);
    }
}
