package com.mockertim.revolut.backendtask;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * In-memory data store to store money transfer transactions data
 */
public class InMemoryTransactionsDatastore {

    private static final Map<String, Transaction> DATASTORE = new ConcurrentHashMap<>();

    public String save(Transaction t) {
        String id = UUID.randomUUID().toString();
        DATASTORE.put(id, t);
        return id;
    }

    public Collection<Transaction> getTransactions() {
        return DATASTORE.values();
    }
}
