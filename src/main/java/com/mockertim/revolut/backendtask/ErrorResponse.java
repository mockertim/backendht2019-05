package com.mockertim.revolut.backendtask;

public class ErrorResponse {

    int code;
    String message;

    public static ErrorResponseBuilder builder() {
        return new ErrorResponseBuilder();
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
