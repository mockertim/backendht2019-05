# BackendHT2019-05

Backend Home Task 2019-05 implementation

## Start the server from command line

 java -jar bht2019-05-jar-with-dependencies.jar

The server starts locally and uses 8080 port by default

If you want to use another port just run

 java -jar bht2019-05-jar-with-dependencies.jar PORT_NUMBER

with desired PORT_NUMBER value

## Perform money transfer

To perform money transfer from one account to another

 curl -X POST http://localhost:8080/api/transfer/account -d \
 '{"recAccount": "00000000000000000000", \
 "senderAccount": "11111111111111111111", \
 "senderName": "John Wick", "currency": "EUR", \
 "sumToTransfer": "123.80"}'

## Get the list of transactions performed so far

 curl -X GET http://localhost:8080/api/transfer/transactions